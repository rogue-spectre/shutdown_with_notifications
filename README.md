Dependencies :
   - espeak      ( voice notification )
     mbrola
     mbrola-fr4
   - libnotify   ( popup notifications - should already be installed )
   - python3-gi  ( last notification )
   
   
Files :
   - shutdown.py
      main file
      handle launch of notifications for all users 
   - notification_shutdown.py
      display notification with notify-send
      user can cancel shutdown
   - last_notification_shutdown.py
     last notification forces user to answer by using a foreground countdown. The user can either cancel or force shutdown.
        
Usage :
   - copy the tree files into a directory of your $PATH. 
   - Launch shutdown.py, a countdown of 10 minutes is set during which visual and voice notifications are sent to
    all users logged in console or X session. Two minutes before shutdown a different notification is sent to X users forcing them to answer.