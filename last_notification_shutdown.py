#!/usr/bin/env python3
# coding=utf8
import os
import psutil
import re
from time import time, sleep
import sys
import psutil
from gi.repository import Notify
from gi.repository import Gtk
import argparse

parser=argparse.ArgumentParser()

parser.add_argument('-d', '--delay',   type=int, required=True)

args=parser.parse_args()
delay_s=int(args.delay)
delay_min=int(args.delay/60)
#args.delay=int(args.delay/60)

def disable_shutdown(a, b):
    print("Annulation de l'extinction")
    os.system('shutdown -c ')

try :
    from gi.repository import Gtk, GObject
    #from gi.repository import Keybinder
    class MyWindow(Gtk.Window):

        def __init__(self, timeout):
            Gtk.Window.__init__(self, title="Extinction automatique")

            #self.activate_focus
            #self.fullscreen()
            self.activate_focus
            self.set_keep_above(True)

            screen_width = 1680

            self.set_border_width(int((screen_width-350)/2))
            self.set_decorated(False)
            self.stick()                     # present on all desktops
            #self.set_default_size(500,100)
            self.set_skip_taskbar_hint(True) # Don't show in taskbar
            self.set_halign(Gtk.Align.CENTER)# GDK_GRAVITY_CENTER
            self.set_valign(Gtk.Align.CENTER)
            #self.maximize()

            self.t0=time()
            self.t=self.t0

            vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
            self.add(vbox)
            
            label = Gtk.Label("Extinction automatique programmée dans moins de " + str(int(timeout/60)) + " minutes")
            
            vbox.pack_start(label, True, True, 0)

            self.progressbar = Gtk.ProgressBar()
            vbox.pack_start(self.progressbar, True, True, 0)

            hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
            vbox.add(hbox)
            
            self.button = Gtk.Button(label="Éteindre Maintenant")
            self.button.connect("clicked", self.on_button_shutdown)
            hbox.pack_start(self.button, True, True, 0)

            button = Gtk.Button("Annuler l'extinction")
            button.connect("clicked", self.on_button_cancel)
            hbox.pack_start(button, True, True, 0)
            
            self.timeout_id = GObject.timeout_add(timeout, self.on_timeout, None)
            self.activity_mode = False

    
        def on_button_shutdown(self, widget):
            os.system('shutdown -c')
            os.system('shutdown -h 0')
            exit(0)
            
        def on_button_cancel(self, widget):
            os.system('shutdown -c ')
            exit(0)

        def on_timeout(self, user_data):
            """
            Update value on the progress bar
            """
            #if self.activity_mode:
            #    self.progressbar.pulse()
            #else:
            #sleep(1)
            t=time()
            #t - self.t0 * 100 / 120 +  self.progressbar.get_fraction()*120
            new_value = (t - self.t0) / (delay_s) #self.progressbar.get_fraction() + 0.1
            chrono = int(delay_s - (t - self.t0))
            self.progressbar.set_text( str(int(chrono)) + ' s')
            self.progressbar.set_show_text( str(int(chrono)) + ' s')
            if new_value > 1:
                exit(0)
                new_value = 0

            self.progressbar.set_fraction(new_value)

            # As this is a timeout function, return True so that it
            # continues to get called
            return True
except :
    print('[ ERROR ] could not define window class')

try :
    #os.system('gshutdown -h 00:01 -c "shutdown" -m')
    win = MyWindow(delay_s)
    #win.quit_remove()
    win.connect("delete-event", disable_shutdown) #disable shutdown on alt+f4     #Gtk.main_quit) # disable alt+f4
    win.show_all()
    Gtk.main()
except :
    print('[ ERROR ] could not define window class')

