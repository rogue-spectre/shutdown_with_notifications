#!/usr/bin/env python3
# coding=utf8
from gi.repository import Notify
from gi.repository import Gtk
import psutil
import os
import argparse

parser=argparse.ArgumentParser()

parser.add_argument('-d', '--delay',   type=int, required=True)
parser.add_argument('-t', '--timeout', type=int, default=30)

args=parser.parse_args()
args.timeout=args.timeout*1000
args.delay=int(args.delay/60)

Notify.init('Extinction Automatique')

notif = Notify.Notification.new(
    'Extinction Automatique', # titre
    '\nDans moins de ' + str(args.delay) + ''' min\nPensez à sauvegarder vos données\n''', # message
    'apport' # icône
)

#notif.set_urgency(Notify.Urgency.CRITICAL)
notif.set_timeout(args.timeout) # 10 seconds

# définition de la callback exécutée lors du clic sur le bouton
def callback(notif_object, action_name, users_data):
    # on peut faire des choses ici
    notif_object.close()
    Gtk.main_quit()
    
def cancel_shutdown(notif_object, action_name, users_data):
    notif_object.close()
    notifok = Notify.Notification.new(
    'Extinction Annulée', # titre
    '', # message
    'dialog-info' # icône
    )
    os.system('shutdown -c')
    os.system('rm -rf  /tmp/shutdown_notif.lock')
    notifok.show()
    Gtk.main_quit()

notif.add_action(
    'our_callback', # identifiant
    '''Annuler l'extinction''', # texte du bouton
    cancel_shutdown, # function callback de notre bouton
    None, # user_datas, ce dont vous avez besoin dans la callback
    None # fonction qui supprime les user_datas
)

try :
    notif.show()
    Gtk.main()
    exit
except :
    exit(1)
