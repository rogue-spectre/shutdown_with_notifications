#!/usr/bin/env python3
import os
import psutil
import re
from time import time, sleep
import sys
from pwd import getpwnam  

timeout=15 # secondes time refresh popup
delay_init=10*60 # minutes * 60 secondes
last_delay=2*60 #
delay=delay_init
shutdown_time=time() + delay_init

#Force kill of espeak / can cause problems if multiples instances locked
try :
    os.system('killall -9 espeak')
except :
    pass

def force_sound() :
    "Force Activation of sound system and increase volume"
    
    for device in 'Master','Headphone','PCM' :
        try :
            # try no unmute sound
            os.system('amixer sset ' + device + '    unmute > /dev/null')
        except :
            pass
    try :
        os.system('amixer cset iface=MIXER,name="Master Playback Volume" 90% >/dev/null')
    except :
        pass

try :
    try :
        try :
            os.system('shutdown -c')
        except :
            print('[ WARNING ] no shutdown to cancel')
        print('shutdown -h ' + str(int(delay/60)))
        os.system('shutdown -h ' + str(int(delay/60)) + '&')
        os.system('touch /tmp/shutdown_notif.lock')
    except :
        print('[ ERROR ] could not launch shutdown')
        exit(1)

    # First voice notification
    if delay_init > ( last_delay + 2*timeout ):
        force_sound()
        try :
            os.system('espeak -v mb-fr4+f2 "Attention ! Extinction automatique de l\'ordinateur programmée dans moins de ' + str(int(delay_init/60)) + ' minutes. Veuillez sauvegarder vos données !" -s 110 -a 200')
        except :
            try :
                os.system('''espeak "Warning ! Automatic shutdown in less than ''' + str(delay_init/60) + ''' minutes. Please backup your datas" -s 110 -a 200 & ''')
            except :
                pass

    # Notification loop
    while True :
        t = time()
        delay = shutdown_time - t 
        
        #
        # Search displays again
        #
        displays=set([(usr.name,usr.terminal) for usr in psutil.get_users() if re.match(':[0-9][0-9]*(\.[0-9][0-9])?',usr.terminal)]
          +  [(usr.name,usr.host)     for usr in psutil.get_users() if re.match(':[0-9][0-9]*(\.[0-9][0-9])?',usr.host)])
        #
        # last voice message
        #
        if delay < last_delay :
            # Notification Sonore
            try :
                force_sound()
                os.system('espeak -v mb-fr4+f2 "Attention ! Extinction automatique dans moins de 2 minutes" -a 200 -s 110')
                #os.system('pico2wave -l=fr-FR -w=/tmp/avertissement.wav "Attention ! Extinction automatique dans moins de 2 minutes"')
                #os.system('aplay /tmp/avertissement.wav')
                #os.system('rm /tmp/avertissement.wav')
            except :
                pass

            # Notification Visuelle
            for usrname,display in displays:
                t = time()
                delay = shutdown_time - t                 
                try :
                    uid = getpwnam(str(usrname)).pw_uid
                    home_dir = str(getpwnam(str(usrname)).pw_dir)
                    #print('DISPLAY=' + str(display) + ' XAUTHORITY=/home/' + str(usrname) + '/.Xauthority' + ' XDG_RUNTIME_DIR=/run/user/' + str(uid) + 
                    #           ' last_notification_shutdown.py  --delay ' + str(int(delay + 60)) + '')
                    #os.system( 'DISPLAY=' + str(display) + ' XAUTHORITY=/home/' + str(usrname) + '/.Xauthority' + ' XDG_RUNTIME_DIR=/run/user/' + str(uid) +
                    #           ' last_notification_shutdown.py  --delay ' + str(int(delay)) + '' )
                    print('DISPLAY=' + str(display) + ' XAUTHORITY=' + home_dir + '/.Xauthority' +
                               ' last_notification_shutdown.py  --delay ' + str(int(delay + 60)) + '')
                    os.system( 'DISPLAY=' + str(display) + ' XAUTHORITY=' + home_dir + '/.Xauthority' +
                               ' last_notification_shutdown.py  --delay ' + str(int(delay)) + '' )                              
                except :
                    print('[ ERROR ] could not notify user in graphical interface')
            
            # end
            exit(0)

        else :
            #
            # launch system notifications
            #
            for usrname,display in displays:
                try :
                    uid = getpwnam(str(usrname)).pw_uid
                    home_dir = str(getpwnam(str(usrname)).pw_dir)
                    #print( 'DISPLAY=' + str(display) + ' XAUTHORITY=/home/' + str(usrname) + '/.Xauthority' + ' XDG_RUNTIME_DIR=/run/user/' + str(uid) +
                    #           ' notification_shutdown.py --timeout ' + str(timeout) + ' --delay ' + str(int(delay + 60)) + '&' )
                    #           
                    #os.system( 'DISPLAY=' + str(display) + ' XAUTHORITY=/home/' + str(usrname) + '/.Xauthority' + ' XDG_RUNTIME_DIR=/run/user/' + str(uid) +
                    #           ' notification_shutdown.py --timeout ' + str(timeout) + ' --delay ' + str(int(delay + 60)) + '&' )
                    print( 'DISPLAY=' + str(display) + ' XAUTHORITY=' + home_dir + '/.Xauthority' + 
                               ' notification_shutdown.py --timeout ' + str(timeout) + ' --delay ' + str(int(delay + 60)) + '&' )
                               
                    os.system( 'DISPLAY=' + str(display) + ' XAUTHORITY=' + home_dir + '/.Xauthority' + 
                               ' notification_shutdown.py --timeout ' + str(timeout) + ' --delay ' + str(int(delay + 60)) + '&' )                               
                except :
                    print('[ ERROR ] could not notify user in graphical interface')
                    
            # notify tty user
            #try :
            #    os.system( 'shutdown -k ' + str(int(delay/60)) )
            #except :
            #    print('[ ERROR ] could not notify user in terminal')

            sleep(timeout + 2) # let 2 seconds between refresh

        if not os.path.exists('/tmp/shutdown_notif.lock') :
            break

except :
    print('ERROR')
    exit(1)
    
exit(0)
